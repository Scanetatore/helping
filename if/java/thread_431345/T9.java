package thread_431345;

import java.awt.EventQueue;
import java.awt.GridLayout;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import java.util.LinkedList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Solution for http://www.inforge.net/xi/threads/aiuto-come-riconoscere-multi-keypressed.431345/
 * Short link: http://z0r.it/4l
 *
 * @author Scanetatore
 *
 */

public class T9 {
    private static JLabel l = new JLabel("waiting...");
    private static int lastKey = -1;
    private static int pressCounter = 0;
    private static Timer timer = new Timer();
    private static int sync = 1;
    private static boolean inWait = true;

    private final static int timeInSeconds = 2;
    
    private final static List<ResetLabelTask> timerTaskList = new LinkedList<ResetLabelTask>();

	public static void main(String args[]) {
		EventQueue.invokeLater(new Runnable() {

			@Override
			public void run() {
				JFrame frame = new JFrame("T9");
				JPanel panel = new JPanel(new GridLayout(1,1));

				frame.addKeyListener(new KeyListener() {

					@Override
					public void keyPressed(KeyEvent e) {
						// The KeyCode is the same of the previous and at least
						// one timer is counting
						
						if(sync == 0)
							sync++;
						
						if(lastKey == e.getKeyCode() && sync > 0) {
							sync++;
						}
						else {
							// Search for the last key pressed timer and run it
							for(ResetLabelTask tt : timerTaskList) {
								if(tt.getKey() == lastKey && tt.getKeyCount() == pressCounter - 1) {
									tt.run();
								}
							}
							
							// Resetting
							timerTaskList.clear();
							timer.cancel();
							timer = new Timer();
							
							pressCounter = 0;
							sync = 1;
							lastKey = e.getKeyCode();
						}

						// We are not waiting, a button was pressed
						inWait = false;

						// Setting text
						l.setText(e.getKeyCode() + " " + pressCounter++ % 3);

						// Set a timer for resetting the label
						ResetLabelTask rlt = new ResetLabelTask(e.getKeyCode(), pressCounter);
						timerTaskList.add(rlt);
						timer.schedule(rlt, timeInSeconds * 1000);
					}

					@Override
					public void keyTyped(KeyEvent e) { }

					@Override
					public void keyReleased(KeyEvent e) { }
				});

				frame.setSize(500, 250);
				frame.add(panel);
				frame.setLocationRelativeTo(null);
				panel.add(l);

				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setVisible(true);
			}
		});
	}

	/**
	 * TimeTask implementation for resetting the label text after a while
	 */
	static class ResetLabel extends TimerTask {
		private static int key = -1;
		private static int keyCount = 0;
		
		public ResetLabel(int key, int count) {
			this.key = key;
			this.keyCount = (count - 1) % 3;
		}
		
		public int getKey() {
			return key;
		}
		
		public int getKeyCount() {
			return keyCount;
		}

		@Override
		public void run() {
			// This timer is finished
			if(sync > 0)
				sync--;

			// All timer are finished and we are not waiting for input
			if(sync == 0 && !inWait) {
				l.setText("waiting...");
				inWait = true;
				
				if(key == lastKey)
					System.out.println(key + " " + keyCount);
			}
		}
	}
}
