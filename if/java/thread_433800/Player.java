package thread_433800;

public class Player {
	private int x;
	private int y;
	private char type;
	
	public Player(int x, int y, char type) {
		this.x = x;
		this.y = y;
		this.type = type;
	}
	
	public int getX() {
		return x;
	}
	
	public int getY() {
		return y;
	}
	
	public char getType() {
		return type;
	}

	public void setX(int x) {
		this.x = x;
	}
	
	public void setY(int y) {
		this.y = y;
	}
	
	public void setType(char type) {
		this.type = type;
	}
}
