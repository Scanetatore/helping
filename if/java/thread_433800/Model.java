package thread_433800;

public class Model {
	private ArrayBackedConfiguration config;
	private Player[] players = new Player[2];
	private static int playerFlag = 0;
	
	public Model(ArrayBackedConfiguration config, Player ...players) {
		this.config = config;
		
		for(int i = 0; i < 2; i++) {
			this.players[i] = new Player(players[i].getX(), players[i].getY(), players[i].getType());
			
			config.setType(players[i].getX(), players[i].getY(), players[i].getType());
		}
	}

	public void move(int direction) {
		Player p = players[getFlag()];
		
		switch(direction) {
		case 2:
			if(p.getX() + 1 < config.getX()) {
				p.setX(p.getX() + 1);
				config.setType(p.getX(), p.getY(), p.getType());
			}
			break;
			
		case 4:
			if(p.getY() - 1 >= 0) {
				p.setY(p.getY() - 1);
				config.setType(p.getX(), p.getY(), p.getType());
			}
			break;
			
		case 6:
			if(p.getY() + 1 < config.getY()) {
				p.setY(p.getY() + 1);
				config.setType(p.getX(), p.getY(), p.getType());
			}
			break;
			
		case 8:
			if(p.getX() - 1 >= 0) {
				p.setX(p.getX() - 1);
				config.setType(p.getX(), p.getY(), p.getType());
			}
			break;
		}
		
		updateFlag();
	}
	
	public void updateFlag() {
		playerFlag = (playerFlag + 1) % 2;
	}
	
	public int getFlag() {
		return playerFlag;
	}

	public void printConfiguration() {
		System.out.println(config);
	}
	
}
