package thread_433800;

public class Main {

	public static void main(String[] args) {
		Controller controller = new Controller(
				new Model(	new ArrayBackedConfiguration(),
							new Player(1, 1, 'o'),
							new Player(1, 3, 'x')
						)
		);
		
		while(true) {
			controller.printGrid();
			controller.takeInput();
		}
	}

}
