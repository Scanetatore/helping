package thread_433800;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Controller {
	private BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
	private Model model;
	private String line;
	
	public Controller(Model model) {
		this.model = model;
	}
	
	public void takeInput() {
		System.out.print("\nInput: ");
		
		try {
			line = in.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		int n = 0;
		
		try {
			n = Integer.parseInt(line);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		}
		
		if(n == 2 || n == 4 || n == 6 || n == 8)
			model.move(n);
	}

	public void printGrid() {
		model.printConfiguration();
	}
	
}
