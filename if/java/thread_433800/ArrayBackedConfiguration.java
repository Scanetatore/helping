package thread_433800;

public class ArrayBackedConfiguration {
	private static int X = 5;
	private static int Y = 5;
	
	private static char[][] grid = new char[X][Y];
	
	public ArrayBackedConfiguration() {
		for(int x = 0; x < X; x++) {
			for(int y = 0; y < Y; y++) {
				grid[x][y] = ' ';
			}
		}
	}
	
	@Override
	public String toString() {
		String str = "";
		
		for(int x = 0; x < X; x++) {
			for(int y = 0; y < Y; y++) {
				str += "[" + grid[x][y] + "]";
			}
			str += '\n';
		}
		
		return str;
	}
	
	public int getX() {
		return X;
	}
	
	public int getY() {
		return Y;
	}
	
	public void setType(int x, int y, char type) {
		grid[x][y] = type;
	}
}
